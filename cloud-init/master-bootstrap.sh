#!/bin/bash

# =================== Configuration =====================

TOKEN="<token>"
IMAGE_CACHE_ENDPOINT="<image-cache-endpoint>"  # Lets us fetch the rkt and docker images a lot quicker (optional)

CLUSTER_DOMAIN="cluster.local"
CLOUD_CONFIG_URL="https://gitlab.com/maikelv/kubernetes-bootstrap/raw/master/cloud-init/master.yaml"
KUBE_MANIFESTS_REPO_URL="https://gitlab.com/maikelv/kubernetes-bootstrap.git"
KUBE_MANIFESTS_REPO_BRANCH="master"

# =======================================================

set -e

# Reboot hook for customization options
if [[ -e /etc/kubernetes/provision/reboot-hook.sh ]];then
    source /etc/kubernetes/provision/reboot-hook.sh
fi

if [[ ! -d /etc/kubernetes/provision ]] || [[ ! -e /etc/kubernetes/provision/lock.txt ]];then

    boot_seconds=$(date +%s)

    # Provision hook for customization options
    if [[ -e /etc/kubernetes/provision/provision-hook.sh ]];then
        source /etc/kubernetes/provision/provision-hook.sh
    fi

    curl \
        --silent \
        --fail \
        --url "${CLOUD_CONFIG_URL}" \
        --output cloud-config.yml

    # Enable kube-bootstrap pod on initial run
    if [[ ! -d /etc/kubernetes/provision ]];then
        sed -E 's|^#\skube-bootstrap(.+)|\1|g' -i cloud-config.yml
        mkdir -p /etc/kubernetes/provision/status
        echo $boot_seconds > /etc/kubernetes/provision/status/boot-seconds.txt
    fi

    sed "s|{token}|${TOKEN}|g" -i cloud-config.yml
    sed "s|{image-cache-endpoint}|${IMAGE_CACHE_ENDPOINT}|g" -i cloud-config.yml
    sed "s|{cluster-domain}|${CLUSTER_DOMAIN}|g" -i cloud-config.yml
    sed "s|{kube-manifests-repo-url}|${KUBE_MANIFESTS_REPO_URL}|g" -i cloud-config.yml
    sed "s|{kube-manifests-repo-branch}|${KUBE_MANIFESTS_REPO_BRANCH}|g" -i cloud-config.yml
    sed 's|{private-ipv4}|$private_ipv4|g' -i cloud-config.yml
    sed 's|{public-ipv4}|$public_ipv4|g' -i cloud-config.yml

    coreos-cloudinit --from-file cloud-config.yml
    mkdir -p /etc/kubernetes/provision
    date > /etc/kubernetes/provision/timestamp.txt
    touch /etc/kubernetes/provision/lock.txt
fi
