#!/bin/bash

# =================== Configuration =====================

MASTER_IPV4="<master-ipv4>"
TOKEN="<token>"
IMAGE_CACHE_ENDPOINT="<image-cache-endpoint>"  # Lets us fetch the rkt and docker images a lot quicker (optional)

CLUSTER_DOMAIN="cluster.local"
CLOUD_CONFIG_URL="https://gitlab.com/maikelv/kubernetes-bootstrap/raw/master/cloud-init/worker.yaml"

# =======================================================

set -e

# Reboot hook for customization options
if [[ -e /etc/kubernetes/provision/reboot-hook.sh ]];then
    source /etc/kubernetes/provision/reboot-hook.sh
fi

if [[ ! -d /etc/kubernetes/provision ]] || [[ ! -e /etc/kubernetes/provision/lock.txt ]];then
    # Provision hook for customization options
    if [[ -e /etc/kubernetes/provision/provision-hook.sh ]];then
        source /etc/kubernetes/provision/provision-hook.sh
    fi

    curl \
        --silent \
        --fail \
        --url "${CLOUD_CONFIG_URL}" \
        --output cloud-config.yml

    sed "s|{master-ipv4}|${MASTER_IPV4}|g" -i cloud-config.yml
    sed "s|{token}|${TOKEN}|g" -i cloud-config.yml
    sed "s|{image-cache-endpoint}|${IMAGE_CACHE_ENDPOINT}|g" -i cloud-config.yml
    sed "s|{cluster-domain}|${CLUSTER_DOMAIN}|g" -i cloud-config.yml
    sed 's|{private-ipv4}|$private_ipv4|g' -i cloud-config.yml
    sed 's|{public-ipv4}|$public_ipv4|g' -i cloud-config.yml

    coreos-cloudinit --from-file cloud-config.yml
    mkdir -p /etc/kubernetes/provision
    date > /etc/kubernetes/provision/timestamp.txt
    touch /etc/kubernetes/provision/lock.txt
fi
