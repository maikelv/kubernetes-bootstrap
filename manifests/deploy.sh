#!/bin/sh

# Deploy Flannel
set-status networking.items.flannel.code 1
kubectl apply -f flannel-configmap.yaml
set-status networking.items.flannel.code 2
kubectl apply -f flannel-daemonset.yaml
set-status networking.items.flannel.code 3

# Deploy Calico
set-status networking.items.calico.code 1
kubectl apply -f calico-daemonset.yaml
set-status networking.items.calico.code 2

# Wait for Flannel
kubectl-rollout-status -f flannel-daemonset.yaml
set-status networking.items.flannel.code 5

# Wait for Calico
kubectl-rollout-status -f calico-daemonset.yaml
set-status networking.items.calico.code 5

# Deploy KubeDNS
set-status networking.items.kube_dns.code 1
kubectl apply -f kube-dns-deployment.yaml
set-status networking.items.kube_dns.code 2
kubectl-rollout-status -f kube-dns-deployment.yaml
set-status networking.items.kube_dns.code 3
kubectl apply -f kube-dns-service.yaml
set-status networking.items.kube_dns.code 5

# Deploy Etcd operator
set-status etcd_migration.items.etcd_operator.code 1
kubectl apply -f etcd-operator-deployment.yaml
set-status etcd_migration.items.etcd_operator.code 2
kubectl-rollout-status -f etcd-operator-deployment.yaml
set-status etcd_migration.items.etcd_operator.code 5

# Deploy Etcd cluster
set-status etcd_migration.items.etcd_cluster.code 1
kubectl-rollout-status thirdpartyresource cluster.etcd.coreos.com
set-status etcd_migration.items.etcd_cluster.code 2
until kubectl get -f etcd-cluster.yaml;do
    until kubectl create -f etcd-cluster.yaml;do
        sleep 5
    done
done
set-status etcd_migration.items.etcd_cluster.code 3
kubectl-rollout-status -f etcd-cluster.yaml
set-status etcd_migration.items.etcd_cluster.code 5

etcd_migrate

# Deploy Heapster
set-status addons.items.heapster.label "Install Heapster"
set-status addons.items.heapster.code 1 "Deploy Heapster"
kubectl apply -f addons/heapster-deployment.yaml
set-status addons.items.heapster.code 5

# Deploy KubeDashboard
set-status addons.items.kube_dashboard.label "Install KubeDashboard"
set-status addons.items.kube_dashboard.code 1 "Deploy KubeDashboard"
sed "s|{token}|${TOKEN}|g" -i addons/kube-dashboard-deployment.yaml
kubectl apply -f addons/kube-dashboard-deployment.yaml
set-status addons.items.kube_dashboard.code 2 "Rollout status KubeDashboard"
kubectl-rollout-status -f addons/kube-dashboard-deployment.yaml
set-status addons.items.kube_dashboard.code 4 "Add KubeDashboard service"
kubectl apply -f addons/kube-dashboard-service.yaml
set-status addons.items.kube_dashboard.url "https://$COREOS_PUBLIC_IPV4:30000"
set-status addons.items.kube_dashboard.code 5 "KubeDashboard available from <a href='https://$COREOS_PUBLIC_IPV4:30000'>https://$COREOS_PUBLIC_IPV4:30000</a>"

