FROM mediamoose/nginx:1.11-9-rc1

RUN apk add --no-cache \
        bash \
        curl \
        git \
        iproute2 \
 && git config --global user.email anonymous \
 && git config --global user.name anonymous \
 && wget -O /usr/bin/jq https://github.com/stedolan/jq/releases/download/jq-1.5/jq-linux64 \
 && chmod +x /usr/bin/jq

ARG KUBECTL_VERSION="1.6.8"
RUN curl \
        --location \
        --url http://storage.googleapis.com/kubernetes-release/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl \
        --output /usr/bin/kubectl \
 && chmod +x /usr/bin/kubectl \
 && kubectl version --client

ENV ETCDCTL_API="3" \
    KUBECONFIG="/kubeconfig"

RUN ln -sf /usr/local/bin/bootstrap /bootstrap \
 && ln -sf /usr/local/bin/status /status \
 && ln -sf /etc/kubernetes/provision/status /etc/nginx/html/status

COPY assets /assets
COPY bin /usr/local/bin
COPY html /etc/nginx/html

CMD ["/bootstrap"]
