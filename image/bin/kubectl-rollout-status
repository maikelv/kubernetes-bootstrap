#!/bin/bash
set -e

resource="$@"
if [[ -z "$resource" ]];then
    echo ""
    echo "Usage:"
    echo ""
    echo "      $0 -f <file>"
    echo "      $0 <kind> <name>"
    echo ""
    exit 1
fi

until kubectl get $@ >/dev/null;do
    echo "Waiting for $@..."
    sleep 1
done

object="$(kubectl get $@ -o json)"
name="$(jq -r '.metadata.name' <<< $object)"
kind="$(jq -r '.kind' <<< $object | tr '[:upper:]' '[:lower:]')"

ready_value=""
scheduled_value=""
ready_filter=".status.numberReady"
scheduled_filter=".status.desiredNumberScheduled"
if [[ $kind == "deployment" ]];then
    ready_filter=".status.availableReplicas"
    scheduled_filter=".status.replicas"
elif [[ $kind == "pod" ]];then
    echo "WARNING: Only checks the first container in a pod."
    ready_value="true"
    scheduled_filter=".status.containerStatuses[0].ready"
elif [[ $kind == "thirdpartyresource" ]];then
    echo "${kind} \"${name}\" is available"
    exit
fi

if [[ $kind == "cluster" ]];then
    while [[ "$(jq -r '.status.members.ready' <<< $object)" == "null" ]];do
        object="$(kubectl get $@ -o json)"
        echo "Waiting for rollout \"${name}\" to finish..."
        sleep 1
    done
else
    ready=0
    while [[ $ready != $scheduled ]];do
        object="$(kubectl get $@ -o json)"
        ready=${ready_value:-$(jq -r $ready_filter <<< $object)}
        scheduled=${scheduled_value:-$(jq -r $scheduled_filter <<< $object)}
        echo "Waiting for rollout \"${name}\" to finish: ${ready} of ${scheduled} are available..."
        sleep 1
    done
fi

echo "${kind} \"${name}\" successfully rolled out"
